<?php

namespace AppBundle\Controller;

use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 *
 * @Route("", methods={"GET"})
 */
class DefaultController extends Controller
{
    /**
     * @Route("", name="accueil")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // Récupération des dates
        $tomorrow = date('Y-m-d', strtotime("+1 days"));
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $todayMinus2 = date('Y-m-d', strtotime("-2 days"));
        $todayMinus3 = date('Y-m-d', strtotime("-3 days"));
        $todayMinus4 = date('Y-m-d', strtotime("-4 days"));
        $todayMinus5 = date('Y-m-d', strtotime("-5 days"));
        $todayMinus6 = date('Y-m-d', strtotime("-6 days"));
        $todayMinus7 = date('Y-m-d', strtotime("-7 days"));

        // Récupération du client pour appeler l'API
        $client = $this->get('eight_points_guzzle.client.api_balldontlie');

        // Appel de l'API pour récupérer tous les matchs de demain jusqu'à aujourd'hui -7 jours
        $response = $client->get('/api/v1/games?dates[]=' . $tomorrow . '&dates[]=' . $today . '&dates[]=' . $yesterday . '&dates[]=' . $todayMinus2 . '&dates[]=' . $todayMinus3 . '&dates[]=' . $todayMinus4 . '&dates[]=' . $todayMinus5 . '&dates[]=' . $todayMinus6 . '&dates[]=' . $todayMinus7 . '&per_page=135');

        // On récupére la data et la meta pour les matchs d'aujourd'hui
        list($data, $meta) = $this->jsonBodyToArray($response);

        // Initialisation de la liste de match
        $totalMatchs = array(
            $tomorrow    => [],
            $today       => [],
            $yesterday   => [],
            $todayMinus2 => [],
            $todayMinus3 => [],
            $todayMinus4 => [],
            $todayMinus5 => [],
            $todayMinus6 => [],
            $todayMinus7 => [],
        );

        foreach ($data as $match) {
            // Pour tous les matchs récupérés grâce à l'API

            // Récupération de la date du match
            $matchDate = date('Y-m-d', strtotime($match['date']));
            // On ajoute le match à la liste de match en fonction de sa date
            array_push($totalMatchs[$matchDate], $match);
        }

        // On rend le template
        return $this->render('index.html.twig', [
            'totalMatchs' => $totalMatchs,
        ]);
    }

    /**
     * @Route("/stats/match/{idMatch}", name="statsMatch", requirements={"idMatch"="\d+"})
     *
     * @param $idMatch
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function matchStatsAction($idMatch)
    {
        // Récupération du client pour appeler l'API
        $client = $this->get('eight_points_guzzle.client.api_balldontlie');

        // Appel de l'API pour récupérer toutes les stats du match
        $responseStats = $client->get('/api/v1/stats?game_ids[]=' . $idMatch . '&per_page=100');

        // Appel de l'API pour récupérer le match
        $responseMatch = $client->get('/api/v1/games/' . $idMatch);

        // On récupére la data et la meta pour les stats du match
        list($stats, $metaStats) = $this->jsonBodyToArray($responseStats);

        // On récupére la data pour le match
        $match = json_decode($responseMatch->getBody()->getContents(), true);

        // On rend le template
        return $this->render('match/stats.html.twig', [
            'stats' => $stats,
            'match' => $match,
        ]);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return array|null
     */
    private function jsonBodyToArray(ResponseInterface $response)
    {
        if ($response->getStatusCode() === 200) {
            // Si la réponse est 200

            // Récupération du contenu du body (array)
            $contentBody = json_decode($response->getBody()->getContents(), true);
            // Récupération de data
            $data = $contentBody["data"];
            // Récupération de meta
            $meta = $contentBody["meta"];


            return [$data, $meta];
        }

        // Si le status code est !== 200, on retourne null
        return null;
    }
}
