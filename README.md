# NBA Stats

L'application NBA Stats permet de voir tous les matchs NBA d'il y a une semaine jusqu'à demain.
Elle permet:
- Si le match n'est pas encore commencé, de voir l'heure de début du match (heure aux USA)
- Si le match est en cours, le score actuel
- Si le match est fini, de voir le score final

Si l'on clique sur un match à partir de l'accueil, on peut voir les stats de chaque joueur qui a joué pendant ce match. Il faut bien sûr sélectionner l'équipe dont on veut voir les stats grâce au toggle bouton.

## Application

### Requis
- PHP >=5.5.9

### Installation

#### Cloner le projet
```bash
$ git clone https://gitlab.com/Zahori/izypay_test.git
```

#### Installer les composants de l'application
```bash
$ composer install
```

#### Installer les assets
Vous devez avoir yarn d'installé sur votre machine
```bash
$ sudo npm install -g yarn
```

Initialiser yarn
```bash
$ yarn install
```

Compiler les assets (l'argument dépend de votre environnement de travail)
```bash
$ ./node_modules/.bin/encore <dev|production>

marche aussi avec:
$ yarn encore dev --watch
```

#### Lancer le serveur (pour le développement)
```bash
$ php bin/console server:run
```

L'application est alors utilisable en local à l'adresse suivante
```
http://localhost:8000
```
